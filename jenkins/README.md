# To build jenkins docker image
	docker build -t jenkins .

# Create a directory in local for jenkins home
  	mkdir ~/jenkins_home

# To run jenkins in a container 
	docker run -it -d --name jenkins -v ~/jenkins_home:/root/jenkins_home -p 8080:8080 jenkins

# Open the browser on http://<hostname>:port

# Jenkins prompts for initial admin password whcih gets created in jenkins home Go to 
	cat ~/jenkins_home/secrets/initialAdminPassword & copy the initialAdminPassword in the browser.