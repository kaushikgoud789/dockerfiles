#DockerFiles Repo

Repository to hold the Dockerfiles needed to spin up servers and other tools.
---

** Tools Needed **
*Hola People*

```
 Visual Studio Code, docker, docker-compose
```
---

** Code Installation **
```
 Choose a folder in your local
 git clone `(source code url)` 
 open VSCode and import this Folder(from root) into VSCode
```
---

** Code Commit **

```
 git add .
 git commit -m "comments"
 git pull
 git push -u origin master
```

** License **

Restricted to A2Nine SRKR Team

** Authors **

 A2Nine team.
