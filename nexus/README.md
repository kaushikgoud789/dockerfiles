

# Create a directory to volume mount the nexus-data inside the container on to the host

	mkdir -p ~/nexus/nexus-data

# Create a network bridge 
  	docker network create nexus

# Command to build nexus docker image
  
 	docker build -t nexus .
  
# Command to run the docker container file

  docker run -d -p 8081:8081 --name nexus -v ~/nexus/nexus-data:/nexus-data --network nexus --hostname nexus nexus

# Access the nexus instance on

	http://<hostname>:8081