# To create a docker image for nginx

	docker build -t alpine-nginx .

# To run nginx in a container which acts as a proxy server to nexus

	docker run -it -d --name alpine-nginx -p 443:443 -v /home/dev/test-nginx/nexus_conf:/etc/nginx/conf.d -v /home/dev/test-nginx/ssl:/etc/ssl/certs --network nexus alpine-nginx

# To access nexus 

	https://<hostname>

